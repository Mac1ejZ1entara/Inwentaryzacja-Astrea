# Meanings of colors:

- <span style="color:yellow"> BTAC OLD </span> 

- <span style="color:green"> BTAC NEW </span> 

- <span style="color:magenta"> PARK ASSIST </span> 

- <span style="color:red"> Empty </span> 

- <span style="color:cyan"> Other </span>

# Repositories:

1. <span style="color:yellow"> AESP-23-CAN-ETH </span> 

*CAN to Ethernet gateway for Astrea Evac System Project*
Software for CAN-ETH converters used in older BTAC version.

2. <span style="color:yellow"> AESP-Audio-Delay-Board-GUI </span> 

C# scripts with tests to implement audio delay on a board.
C# GUI for testing the delay boards used in older BTAC version.

3. <span style="color:green"> aesp-buildroot </span> 

Clone of a different repository with a tool to generate embedded Linux systems through cross-compilation.

4. <span style="color:green"> aesp-buildscripts </span> 

Complete kernel building makefile.

5. <span style="color:yellow"> AESP-CAN-ETH-QT-SNIFFER </span> 

*CAN messages sniffer/sender written in QT*

6. <span style="color:cyan"> AESP-ci-system </span> 

The 'bare-repositories' in CD defrost. Contains teamcity-docker-compose files.

7. <span style="color:yellow"> AESP-db-peq-k60 </span> 

Kinetis Parametric EQualizer software. Used in older BTAC version.

8. <span style="color:red"> AESP-jenkins-configuration </span> 

Empty folder.

9. <span style="color:green"> aesp-linux-toradex </span> 

Embedded Linux distribution.

10. <span style="color:red"> aesp-logic </span> 

*Decision making logic for Audio Evacuation System Project (AESP)* <br>
Empty folder.

11. <span style="color:yellow"> AESP-MarvellConfigurationTool </span> 

*Repository for configuring Marvell boards in ASTREA project.*
In README there are some information on packages and applications that wouldn't work in defrost/AESP-CI from CD.

12. <span style="color:yellow"> AESP-marvell-sys </span> 

*Repository with the kernel and rootfs for Marvell boards.*
Marvell dev boards (MV-L200808-10) were used in older version of BTAC before IMX-based Master Boards were complete. 

13. <span style="color:yellow"> AESP-marvell-user-space </span> 

Whole loop system testing application. For use with Marvell dev boards (MV-L200808-10). 

14. <span style="color:red"> AESP-PanelAudioController </span> 

Empty repository.

15. <span style="color:green"> aesp-super </span> 

*AESP TOP LEVEL REPOSITORY*
Used to download all necessary repositories.
These repositories contain software for the IMX7-based Master Board used in BTAC system.

16. <span style="color:green"> aesp-u-boot-toradex </span> 

Uboot Boot loader.
For IMX7 Master Boards.

17. <span style="color:green"> aesp-userspace </span> 

CAN simulator, file system overlay and more.
For IMX7 Master Boards.

18. <span style="color:cyan"> astrea-ci-teamcity </span> 

*This repository is used to hold the settings of running TeamCity CI instance. It is here in case there is no TeamCity backup file created by the web interface.*

19. <span style="color:magenta"> PAAD </span> 

Code related to ParkAssist project, most likely used by the Auto Focus device.