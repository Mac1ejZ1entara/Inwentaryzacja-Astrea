# Directory tree

```
400_BTAC_ASTREA
    - 04_Astrea - Contains some subdirectories related to Park Assist Autofocus Device, they're all empty
    - 20_Btac Solutions - Astrea
        - 10_DelayBoard
        - 21_MarvellSoftware
        - 22_ToradexColibriiMX7
        - 30_BtacSystem
        - 40_InfraSystem
        - 50_AltiumHWProjects
        - 60_AstreaAVBStack
        - 70_UnboxinDocumentation
        - 90_MiscDocs
        - 99_Outdated
```

# 10_DelayBoard
Contains a datasheet for the Cirrus Logic CS42448 CODEC device. Device contains  six 24-bit ADC's and eight DAC's.

# 21_Marvell Software
Contains archives in .tar format with disk images of Marvell AVB software package along with documentation. Also contains archives with source code of the software.

# 22_ToradexColibriiMX7
Contains datasheet of Toradex Colibri Evaluation Board and Toradex Pinout Designer Tool

# 30_Btac System
Contains saved .html pages leading to draw.io. When we tried to access them, we got a message "The file was not found. It does not exist or you do not have access. (Kacper Lesnianski, kacper.lesnianski@thaumatec.com)"

The directory also contains *Standards and norms* subdirectory which contains three documents with European standards for fire detection and fire alarm systems for buildings.

# 40_Infra System
Empty (The directory, not the section in document :) )

# 50_Altium HW Projects
Contains files with hardware projects designed in Altium.
Some folders also include schematics/descriptions.

Subdirectories:
- AP1504-3 Delay Board - Altium
- AP1601-0 Infra 5x 220W amp and detection - Altium + Simulation results + pdf schematic
- AP1601-0 Infra (13-7-2016 17-05-17) Amplifier SCH ONLY - Altium
- Backpanel_PCB (27-7-2016 12-16-06) SCH ONLY - Altium
- BTAC_ControllerGlass rev0 NOT TESTED - Altium + pdf schematic + pdf with 3d render + GERBER + Datasheets
  Datasheets include: ksz8041nl and ksz8041tl Transceivers datasheets (and their schematics), pcb design guidelines and a document describing capacitive coupling of Ethernet transceivers without using transformers.
- BTAC_Front LEDs Rev1 - Altium + Schematics
- BTAK_AB NOT COMPLETED - Altium
- BTAK_amplifier REV2 - Altium
- BTAK_IMXBOARD REV1 - Altium + Schematics
- BTAK_IMXBOARD REV2 - Altium + Schematics + GERBER
- BTAK_MAINBOARD MARVELL COMPLETE - Altium
- BTAK_mainboard_old MARVELL KINETIS NOT COMPLETED - Altium
- BTAK_mainboard_old PIGGYBACK NOT COMPLETED - Altium
- BTAK MAINBOARD PIGGYBACK NOT COMPLETED - Altium + Schematic
- BTAK PIGGYBACK - Altium + Schematic
- BTAK_piggyback evalboard - Altium
- BTAK_routerboard REV1 - Altium + Schematics + pdf with 3d render + Datasheets of ksz8041nl and ksz8041tl Transceivers
- BTAK_routerboard REV1 COMPLETE - Altium
- Front PCB Rev1 (7-9-2016 11-23-46) - Altium
- Libs - Some libraries i guess?
- PAAD - source code, an executable and a schematic of a camera holder
- SW - PCB designs, compiler and build environment and some confidential documentation from Marvell
       Altium files + Schematics + source code + binary for Backpanel
- Toradex Colibri iMX7 - datasheets and schematic of the Colibri Evaluation board, excel sheets with pin configuration, Toradex Pinout Designer tool
- VE1505 NOT COMPLETED - Altium
- VE1507-0 - Altium + Schematic
- VE1507-1_Base (14-7-2016 08-32-30) NOT COMPLETED - Altium
- VE1507-1 Infra Base NOT COMPLETED - Altium + Document with description of the Infra tunnel system + Datasheets
  Datasheets include: 
  - 3V PSRAM memory datasheet
  - LAN Transformer WE-LAN datasheet
  - Freescale K60P144M100SF2V2 datasheet
  - ON MC74VHC373 Octal D-Type Latch with 3-State Output datasheet
- VE1604-0 LedFront Brick NOT COMPLETED - Altium + Schematics + Gerber + Front Brick
  Indicators and Microphone document (Hardware description)

# 60_AstreaAVB Stack
Saved html pages leading to draw.io projects we can't access

# 70_Unboxing Documentation
Pdf file with detailed description of CAN communication between bricks.
Also includes RunningBtakdSimulator.md file which contains instructions how to run btakd simulator
also an md folder with some files i can't open (xml-like format).

# 90_MiscDocs
Contains:
- Astrea-FTP-Site.txt - URL and credentials of Astrea FTP server
- btak-rack.html - draw.io project we can't access
- DRP Panel.pdf - DRP Panel Implementation Notes
- system_overview.pdf - System Overview Compliance with EN 54-16 for Astrea System
- mvl - documentation from Marvell
    Includes:
    - Physical Layer Compliance Testing for 100BASE-TX Application Note
    - Port SGMII to Fiber/Copper Auto-media Select Reference Design
    - Link Street ® 88E6352 Datasheet 7 Port Gigabit AVB Switch with Energy Efficient Ethernet (EEE)
    - 88E7221-128 Pin Reference Design Board
    - 88E7251 Rev1.0 Block Diagram
    - LVPECL / LVDS Termination
    - 88E3082 RMII TO FIBER
    - 88E3015 EVALUATION BOARD
    - 88E3015 Reference Schematics 3.3V RGMII/MII to Copper/Fiber
    - 88E1043 Datasheet - Integrated Quad Gigabit Ethernet Fiber Transfer
    - Link Street® 88E7221/88E7251/88E7251F Datasheet Arm 9 CPU with FE Switch, PHYs and Audio Ports with Full AVB Support
    - Link Street ® 88E7221/88E7251/88E7251F - CPU Functional Specification
    - Link Street ® 88E7251/88E7251F Switch Core and PHYs Functional Specification
    - Link Street ® 88EA6352 Datasheet 7 Port Automotive AVB Switch with Energy Efficient Ethernet (EEE)
    - 88E3082 100BASE-FX Connections
    - Application Note - Fast Ethernet PHYs - FAQ
    - SFP Applications - Ethernet System Design Considerations
    - 88E1119R GMII/MII Interface Series Terminations
    - Debug Checklist for Marvell Ethernet PHYs
    - Auto-Neg/MAC Interface

# 99_Outdated
Brainstorming - Astrea Project Status Dump.xmind