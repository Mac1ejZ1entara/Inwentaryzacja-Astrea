# Defrost:
1. AESP-CI

Encountered problems with ```docker-compose build``` (or ```docker-compose up```, both instruction do the same).
Probably the reason for this problem is that the admin privilege policy in TeamCity agent Docker changed in version 2020.1. The commands that require 'sudo' before them don't want to execute.

2. AESP-GUI

Docker worked, additionally in defrost/aesp-gui/README.md we managed to execute 'Verifying the production deployment', up to 'Manual procedure'. Thanks to this we have a working GUI demo.

The makefile in GUI_WORK/aesp-gui does not work (problems with packages, incompatible versions).
The ```make build``` command returns errors for the ```build worker``` (problem with npm) and the ```build meteor``` (problem with meteor).

3. AESP-MASTER-BOARD

The compilation attempt ends with an error:
```
fseeko.c:109:4: error: #error "Please port gnulib fseeko.c to your platform! Look at the code in fseeko.c, then report this to bug-gnulib."
freadahead.c: In function 'freadahead':
freadahead.c:91:3: error: #error "Please port gnulib freadahead.c to your platform! Look at the definition of fflush, fread, ungetc on your system, then report this to bug-gnulib."
   91 |  #error "Please port gnulib freadahead.c to your platform! Look at the definition of fflush, fread, ungetc on your system, then report this to bug-gnulib."
      |   ^~~~~
```
Most likely the problem is that the m4 package uses an outdated version of gnulib. (https://github.com/sifive/freedom-u-sdk/issues/74)

Instructions:
```
sudo apt-get install build-essential
sudo apt-get build-dep m4
```
also didn't help.


4. AESP-ROUTER-BOARD

- package lib32z1 required to run arm-none-linux-gnueabi-gcc <br>
If it's missing we get an "No file or directory" error when trying to run it

- u-boot-tools required to build router board kernel

- can't get rootfs to compile right now <br>
   ```make rootfs``` results in error:
   ```
   libgcc_s.so.1 must be installed for pthread_cancel to work
   Aborted (core dumped)
   ```
   Installed libgcc-s1-i386-cross and libgcc-10-dev-i386-cross - still didn't work

- ```make kernel_compile bbb_compile``` - works

- ```make uboot``` - errors during compilation (didn't work before freezing so that's expected, version from bitbucket repository also didn't work)


## Required packages:
```
#Downloaded for CI and GUI
sudo apt-get -y install docker postgresql tigervnc-viewer npm libzmq3-dev
sudo apt install python3-pip curl
sudo pip install docker-compose
curl https://install.meteor.com/ | sh
curl -fsSL https://deb.nodesource.com/setup_current.x | sudo -E bash -
sudo apt-get install nodejs
```


# Hardware:

1. AP1504-3 Delay Board

2. AP1601-0 Infra 5x 220W amp and detection

3. AP1601-0 Infra (13-7-2016 17-05-17) Amplifier SCH ONLY

4. Backpanel_PCB (27-7-2016 12-16-06) SCH ONLY

5. BTAC_ControllerGlass rev0 NOT TESTED

6. BTAC_Front LEDs Rev1

7. BTAK_AB NOT COMPLETED

8. BTAK_amplifier REV2

9. BTAK_IMXBOARD REV1

10. BTAK_IMXBOARD REV2

11. BTAK_MAINBOARD MARVELL COMPLETE

12. BTAK_mainboard_old MARVELL KINETIS NOT COMPLETED

13. BTAK_mainboard_old PIGGYBACK NOT COMPLETED

14. BTAK MAINBOARD PIGGYBACK NOT COMPLETED

15. BTAK PIGGYBACK

16. BTAK_piggyback evalboard

17. BTAK_routerboard REV1

18. BTAK_routerboard REV1 COMPLETE

19. Front PCB Rev1 (7-9-2016 11-23-46)

20. Libs

21. PAAD

22. SW

23. Toradex Colibri iMX7

24. VE1505 NOT COMPLETED

25. VE1507-0

26. VE1507-1_Base (14-7-2016 08-32-30) NOT COMPLETED

27. VE1507-1 Infra Base NOT COMPLETED

28. VE1604-0 LedFront Brick NOT COMPLETED

