AESP - Astrea Evacuation System Project

A project related to the sound system, such as the announcement of an alarm in the tunnel, where over a long distance there are many loudspeakers connected.
Currently the loudspeakers are connected in series, which means that a cable failure at a given point cuts off many loudspeakers.
An idea to solve this problem is to connect the loudspeakers in a ring network - that is, data can be transmitted from two directions.
With this solution, a single cable failure can cut off at most one speaker.

This solution requires a delay in the playback of the recording so that there is no echo 
(some speakers receive the data faster than others, so they have to wait until all (?) receive the data before they can play back the recording).


The main problem was encountered with the network card and this was the main reason why the project was halted. (?)

Main hardware components:
- master board - delay and main program
- router board - stability and keeping the speakers connected
